from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings

from katyfuks.views import ExtraContextTemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += i18n_patterns('',
    url(r'^become_consultant/',
        ExtraContextTemplateView.as_view(
            template_name="become_consultant.html",
            extra_context={'active_menu': "become_consultant"}
        ),
        name="become_consultant"
    ),

    url(r'^contact/',
        ExtraContextTemplateView.as_view(
            template_name="contact.html",
            extra_context={'active_menu': "contact"}
        ),
        name="contact"
    ),

    url(r'^opinions/', include('opinions.urls')),

    url(r'^', include('catalogue.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
