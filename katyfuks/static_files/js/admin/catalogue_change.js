$(document).ready(function() {
    rePosition();

    if($('.deletelink').length) {
        $('#page_set-group .inline-related:not(.empty-form)').addClass('old-form');

        $('.old-form').each(function() {
            var file_url = $(this).find('.field-image').find('a').attr('href');
            var file_name = file_url.split("/");
            file_name = file_name[file_name.length - 1];
            $(this).find('.field-image').html("");
            $(this).find('.field-image').append('<img class="ajax_thumb" src="'+ file_url +'" /><span class="ajax_file_name">'+ file_name +'</span>');
            $(this).find('.field-position').hide();
            $(this).find('.delete').children('label').remove();
            $(this).find('.delete').before('<span class="ajax_delete_button inline-deletelink"></span>');
        });

        $('.add-row').remove();

        $('#page_set-group').prepend('<table class="ajax_upload"><tr><td><span class="ajax_add_button">Add Pages<input type="file" multiple="multiple" /></span><span class="ajax_upload_button">Upload changes</span><span class="ajax_reverse_button">Reverse order</span><span class="ajax_az_button">A-Z</span><span class="ajax_za_button">Z-A</span><span class="ajax_delete_selected">Delete selected</span><span class="ajax_unselect_all">Unselect all</span><span class="ajax_select_all">Select all</span></td></td></tr></table>');

        $('.ajax_select_all').click(function() {
            $('input[id$=DELETE]').prop('checked', 'checked');
        });
        $('.ajax_unselect_all').click(function() {
            $('input[id$=DELETE]').removeAttr('checked');
        });

        $('.ajax_add_button input').change(function() {
            var files = this.files;
            smth_is_changed = true;
            for( x in files ) {
                if(typeof files[x] == 'object') {
                    var fr = new FileReader();
                    fr.file = files[x];
                    fr.onloadend = showFileInList;
                    fr.readAsDataURL(files[x]);
                }
            }
        });

        $('.ajax_reverse_button').click(function() {
            $('#page_set-group .inline-related:not(.empty-form)').each(function() {
                $(this).insertAfter($(this).parent().children('h2'));
            });

            rePosition();
            smth_is_changed = true;

            return false;
        });

        $('.ajax_az_button').click(function() {
            $('#page_set-group .inline-related:not(.empty-form)').sort(naturalSort).insertAfter($('#page_set-group').children('h2'));

            rePosition();
            smth_is_changed = true;

            return false;
        });

        $('.ajax_za_button').click(function() {
            $('#page_set-group .inline-related:not(.empty-form)').sort(naturalSortR).insertAfter($('#page_set-group').children('h2'));

            rePosition();
            smth_is_changed = true;

            return false;
        });

        $('#page_set-group').sortable({
            appendTo: 'parent',
            cursor: 'move',
            items: '> .inline-related:not(.empty-form)',
            revert: true,
            scroll: true,
            tolerance: 'pointer',
            placeholder: 'sortable-placeholder',
            forcePlaceholderSize: true,
            update: function(event, ui) {
                smth_is_changed = true;
                rePosition();
            }
        });

        $('.ajax_delete_selected').click(function() {
            $('<div class="overlay"><div class="uploading_status"><span class="progress_old_text"></span><span class="progress_old_bar"><span class="upload_old_counter"></span></span><span class="uploading_finished">Zakończono</span><button class="upload_again">Delete failed</button></div></div>').appendTo('body');

            $('.uploading_status').css({
                top: ($(window).height() - $('.uploading_status').outerHeight(true)) / 2,
                left: ($(window).width() - $('.uploading_status').outerWidth(true)) / 2,
            });

            $('.upload_again').click(function() {
                $('.overlay').remove();
                $('.ajax_delete_selected').trigger('click');
                return false;
            });

            var checked_pages = $('input[id$=DELETE]:checked');
            var xNewTotal = checked_pages.length;
            $('.progress_new_text').text('Deleting: 0 / ' + xNewTotal);

            var x = 0;
            checked_pages.each(function() {
                var field_set = $(this).parents('.inline-related');
                if($(this).attr('data-id')) {
                    delete formList[$(this).attr('data-id')];
                    field_set.animate({
                            width: 0
                        },
                        500,
                        function() {
                            field_set.remove();
                            rePosition();
                        }
                    );

                    x += 1;
                    percentage = Math.floor((x / xNewTotal) * 100);
                    $('.upload_old_counter').css({width: percentage + "%"});
                    $('.progress_old_text').text('Deleting: ' + x + ' / ' + xNewTotal);
                } else {
                    $.ajax({
                        async: false,
                        type: "post",
                        url: "/admin/catalogue/catalogue/upload_image/",
                        data: {
                            old: 1,
                            delete_page: 1,
                            page_id: field_set.find('input[name$=-id]').val(),
                            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
                        },
                        success:    function(value){
                            field_set.animate({
                                    width: 0
                                },
                                500,
                                function() {
                                    field_set.remove();
                                    rePosition();
                                }
                            );

                            x += 1;
                            percentage = Math.floor((x / xNewTotal) * 100);
                            $('.upload_old_counter').css({width: percentage + "%"});
                            $('.progress_old_text').text('Deleting: ' + x + ' / ' + xNewTotal);
                        }
                    });
                }
            });

            if(x >= xNewTotal) {
                $('.overlay').remove();
            } else {
                alert("x: " + x + " total: " + xNewTotal);
                $('.upload_again').show();
                $('.uploading_finished').text('Deleting finished, with errors!').css({ display: 'block'});
            }
        });

        $('.old-form .ajax_delete_button').click(function() {
            var field_set = $(this).parents('.old-form');

            $.ajax({
                type: "post",
                url: "/admin/catalogue/catalogue/upload_image/",
                data: {
                    old: 1,
                    delete_page: 1,
                    page_id: field_set.find('input[name$=-id]').val(),
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
                },
                success:    function(value){
                    field_set.animate({
                            width: 0
                        },
                        500,
                        function() {
                            field_set.remove();
                            rePosition();
                        }
                    );
                }
            });
        });

        $('.ajax_upload_button').click(function() {
            $('<div class="overlay"><div class="uploading_status"><span class="progress_old_text"></span><span class="progress_old_bar"><span class="upload_old_counter"></span></span><span class="progress_new_text"></span><span class="progress_new_bar"><span class="upload_new_counter"></span></span><span class="uploading_finished">Zakończono</span><button class="upload_again">Upload failed files</button></div></div>').appendTo('body');

            $('.uploading_status').css({
                top: ($(window).height() - $('.uploading_status').outerHeight(true)) / 2,
                left: ($(window).width() - $('.uploading_status').outerWidth(true)) / 2,
            });

            $('.upload_again').click(function() {
                $('.overlay').remove();
                $('.ajax_upload_button').trigger('click');
                return false;
            });

            var xNewTotal = Object.keys(formList).length;
            $('.progress_new_text').text('Uploading new: 0 / ' + xNewTotal);

            var xOldTotal = $('#page_set-group .old-form').length;
            $('.progress_old_text').text('Uploading changes: 0 / ' + xOldTotal);

            var upload_old_status = uploadOld();
            var upload_queue_status = uploadQueue();

            if(upload_queue_status && upload_old_status) {
                $('.overlay').remove();
                smth_is_changed = false;
            } else {
                smth_is_changed = true;
                $('.upload_again').show();
                $('.uploading_finished').text('Upload finished, with errors!').css({ display: 'block'});
            }

            return false;
        });

        $('form').submit(function() {
            if(smth_is_changed) {
                if(confirm('You have unsaved image changes. If You click "ok", some changes may be lost.')) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }
});

var smth_is_changed = false;

function rePosition() {
    var x = 1;
    $('#page_set-group .inline-related:not(.empty-form)').each(function() {
        $(this).find('h3').find('.inline_label').text('#' + x);
        $(this).find('.field-position').find('input').val(x);
        x = x + 1;
    });
    $('#page_set-empty').find('.field-position').find('input').val(x);
    $('#page_set-empty').find('h3').find('.inline_label').text('#' + x);
}

function uploadOld() {
    var x = 0;
    var xTotal = $('#page_set-group .old-form').length;
    var percentage = Math.floor((x / xTotal) * 100);

    $('.progress_old_text').text('Uploading changes: ' + x + ' / ' + xTotal);

    $('#page_set-group .old-form').each(function() {
        $.ajax({
            type: "post",
            async: false,
            url: "/admin/catalogue/catalogue/upload_image/",
            data: {
                old: 1,
                page_id: $(this).find('input[name$=-id]').val(),
                position: $(this).find('.field-position').find('input').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },
            success:    function(value){
            }
        });
        x += 1;
        percentage = Math.floor((x / xTotal) * 100);
        $('.upload_old_counter').css({width: percentage + "%"});
        $('.progress_old_text').text('Uploading changes: ' + x + ' / ' + xTotal);
    });

    if(x >= xTotal) {
        return true;
    } else {
        return false;
    }
}

var formList = new Object();

var showFileInList = function (ev) {
    var file = ev.target.file;
    if (file && (typeof formList[file.name] == "undefined") ) {
        var pos = parseInt($('#page_set-empty').find('.field-position').find('input').val());
        var fieldset = $('#page_set-empty').clone().insertBefore($('#page_set-empty'));
        fieldset.attr('id', '').removeClass('empty-form').addClass('new-form').show();

        fieldset.find('h3').append('<span class="ajax_delete_button inline-deletelink"></span>');
        fieldset.find('h3').append('<span class="delete"><input type="checkbox" id="id_page_set-DELETE" name="page_set-DELETE" data-id="'+ file.name +'" /></span>');
        fieldset.find('.field-position').hide();
        $('#page_set-empty').find('.field-position').find('input').val(pos + 1);
        $('#page_set-empty').find('h3').find('.inline_label').text('#' + (pos + 1));
        fieldset.find('.ajax_delete_button').bind('click', function() {
            delete formList[file.name];
            fieldset.animate({
                    width: 0
                },
                500,
                function() {
                    $(this).remove();
                    rePosition();
                }
            );
            return false;
        });
        if (file.type.search(/image\/.*/) != -1) {
            var thumb = $('<img class="ajax_thumb" src="'+ ev.target.result +'" />');
            fieldset.find('.field-image').html("");
            fieldset.find('.field-image').prepend(thumb).append('<span class="ajax_file_name">'+ file.name +'</span>');
            thumb.on('mouseover', showImagePreview);
            thumb.on('mouseout', removePreview);
        }

        formList[file.name] = {
            file: file,
            fieldset: fieldset,
            file_src: ev.target.result
        };
    }
}
var preview = null;

var showImagePreview = function (ev) {
    var div = $('<div class="imagePreview"></div>');
    var div_width = div.css('width').replace('px', '');
    var div_height = div.css('height').replace('px', '');
    var window_width = $(window).width();
    var div_left_pos = ev.pageX + 30;
    if( (parseInt(ev.pageX) + parseInt(div_width)) > parseInt(window_width) ) {
        div_left_pos = (ev.pageX - div_width) - 30;
    }
    div.css({
        top: (ev.pageY - (div_height / 2)) + "px",
        left: div_left_pos + "px",
        opacity: 0,
    });

    var img = $('<img src="'+ ev.target.src +'" />');
    div.append(img);
    $('body').append(div);
    document.body.addEventListener("mousemove", movePreview, false);
    preview = div;
    div.animate({opacity: 1}, 250);
}

var movePreview = function (ev) {
    if (preview) {
        var preview_width = preview.css('width').replace('px', '');
        var preview_height = preview.css('height').replace('px', '');
        var window_width = $(window).width();
        var preview_left_pos = (ev.pageX + 30);
        if( (parseInt(ev.pageX) + parseInt(preview_width)) > parseInt(window_width) ) {
            preview_left_pos = (ev.pageX - preview_width) - 30;
        }
        preview.css({
            top: (ev.pageY - (preview_height / 2)) + "px",
            left: preview_left_pos + "px",
        });
    }
}

var removePreview = function (ev) {
    document.body.removeEventListener("mousemove", movePreview, false);
    preview.remove();
}

var uploadQueue = function () {
    var x = 0;
    var xTotal = Object.keys(formList).length;
    var percentage = Math.floor((x / xTotal) * 100);

    $('.progress_new_text').text('Uploading new: ' + x + ' / ' + xTotal);

    for( key in formList ) {
        var item = formList[key];
        if(item.fieldset.find('.loader').length) {
            item.fieldset.find('.loader').text('Uploading...');
        } else {
            $('<div class="loader">Uploading...</div>').appendTo(item.fieldset);
        }

        var up_state = uploadFile(item.file, item.fieldset, item.file_src);

        if(up_state) {
            delete formList[key];
            x += 1;
            percentage = Math.floor((x / xTotal) * 100);
            $('.upload_new_counter').css({width: percentage + "%"});
            $('.progress_new_text').text('Uploading new: ' + x + ' / ' + xTotal);
        }
    }

    if(x >= xTotal) {
        return true;
    } else {
        return false;
    }
}

var uploadFile = function (file, fieldset, src) {
    if (fieldset && file) {
        $.ajax({
            type: "post",
            async: false,
            url: "/admin/catalogue/catalogue/upload_image/",
            data: {
                file: src,
                file_name: file.name,
                position: fieldset.find('.field-position').find('input').val(),
                catalogue: $('#id_page_set-__prefix__-catalogue').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },
            success:    function(value){
                var div = fieldset.find(".loader");

                if(value == "Error") {
                    div.css({width: "100%", "background-color": "#f00", color: "#DDD"});
                    div.text("Upload error");
                } else {
                    div.css({width: "100%", "background-color": "#0f0", color: "#3DD13F"});
                    div.text("Upload complete");
                    fieldset.removeClass('new-form').addClass('old-form');
                    var field_id = fieldset.find('#id_page_set-__prefix__-id');
                    field_id.val(value);
                    field_id.attr('id', field_id.attr('id').replace('__prefix__', value)).attr('name', field_id.attr('name').replace('__prefix__', value));
                }
            },
            error:      function(){
                var div = fieldset.find(".loader");
                div.css({width: "100%", "background-color": "#f00", color: "#DDD"});
                div.text("Upload error");
            },
            xhrFields: {
                onprogress: function (progress) {
                    var loader = fieldset.find(".loader");
                    var percentage = Math.floor((progress.total / progress.totalSize) * 100);
                    loader.css({width: percentage + "%"});
                    loader.text("Uploading: "+ percentage + "%");
                }
            }
        });
    }

    return fieldset.find('input[id$=-id]').val();
}

function naturalSort (a, b) {
    a = $(a).find('.ajax_file_name').text();
    b = $(b).find('.ajax_file_name').text();
    var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
        sre = /(^[ ]*|[ ]*$)/g,
        dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
        hre = /^0x[0-9a-f]+$/i,
        ore = /^0/,
        i = function(s) { return naturalSort.insensitive && (''+s).toLowerCase() || ''+s },
        // convert all to strings strip whitespace
        x = i(a).replace(sre, '') || '',
        y = i(b).replace(sre, '') || '',
        // chunk/tokenize
        xN = x.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
        yN = y.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
        // numeric, hex or date detection
        xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
        yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
        oFxNcL, oFyNcL;
    // first try and sort Hex codes or Dates
    if (yD)
        if ( xD < yD ) return -1;
        else if ( xD > yD ) return 1;
    // natural sorting through split numeric strings and default strings
    for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
        // find floats not starting with '0', string or 0 if not defined (Clint Priest)
        oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
        oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
        // handle numeric vs string comparison - number < string - (Kyle Adams)
        if (isNaN(oFxNcL) !== isNaN(oFyNcL)) { return (isNaN(oFxNcL)) ? 1 : -1; }
        // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
        else if (typeof oFxNcL !== typeof oFyNcL) {
            oFxNcL += '';
            oFyNcL += '';
        }
        if (oFxNcL < oFyNcL) return -1;
        if (oFxNcL > oFyNcL) return 1;
    }
    return 0;
}

function naturalSortR(a, b) {
    a = $(b).find('.ajax_file_name').text();
    b = $(a).find('.ajax_file_name').text();
    var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
        sre = /(^[ ]*|[ ]*$)/g,
        dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
        hre = /^0x[0-9a-f]+$/i,
        ore = /^0/,
        i = function(s) { return naturalSort.insensitive && (''+s).toLowerCase() || ''+s },
        // convert all to strings strip whitespace
        x = i(a).replace(sre, '') || '',
        y = i(b).replace(sre, '') || '',
        // chunk/tokenize
        xN = x.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
        yN = y.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
        // numeric, hex or date detection
        xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
        yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
        oFxNcL, oFyNcL;
    // first try and sort Hex codes or Dates
    if (yD)
        if ( xD < yD ) return -1;
        else if ( xD > yD ) return 1;
    // natural sorting through split numeric strings and default strings
    for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
        // find floats not starting with '0', string or 0 if not defined (Clint Priest)
        oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
        oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
        // handle numeric vs string comparison - number < string - (Kyle Adams)
        if (isNaN(oFxNcL) !== isNaN(oFyNcL)) { return (isNaN(oFxNcL)) ? 1 : -1; }
        // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
        else if (typeof oFxNcL !== typeof oFyNcL) {
            oFxNcL += '';
            oFyNcL += '';
        }
        if (oFxNcL < oFyNcL) return -1;
        if (oFxNcL > oFyNcL) return 1;
    }
    return 0;
}
