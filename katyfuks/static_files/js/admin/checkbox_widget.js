$(document).ready(function() {
    $('ul.parent > li').each(function() {
        if($(this).children('ul').length) {
            var expand_text = 'rozwiń';
            if($(this).children('ul').find('input:checked').length) {
                expand_text = 'zwiń';
                $(this).children('ul').show();
            }
            $(this).children('.option_header').addClass('clearfix').append('<span class="expand">'+expand_text+'</span>');
        }
    });

    $('.option_header .expand').click(function() {
        var expand_button = $(this);
        expand_button.parent().next().slideToggle('slow', function() {
            if($(this).is(':visible')) {
                expand_button.text('zwiń');
            } else {
                expand_button.text('rozwiń');
            }
        });
    });
});
