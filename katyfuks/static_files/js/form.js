$(document).ready(function() {
    var form = $('.expert_application form');

    form.submit(function() {
        var email = $.trim($('#email').val());
        var imie = $.trim($('#imie').val());
        var nazwisko = $.trim($('#nazwisko').val());
        var firma = $.trim($('#firma').val());
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        if( imie.length == 0 ){
            $('#imie').val("");
            $('#blad_imie').show().animate({opacity:1},3000).animate({opacity:0},1000,'linear',function(){$(this).hide().css('opacity',1)});
            return false;
        }
        if( nazwisko.length == 0 ){
            $('#nazwisko').val("");
            $('#blad_nazwisko').show().animate({opacity:1},3000).animate({opacity:0},1000,'linear',function(){$(this).hide().css('opacity',1)});
            return false;
        }
        if( email.length == 0 ){
            $('#email').val("");
            $('#blad_email').show().animate({opacity:1},3000).animate({opacity:0},1000,'linear',function(){$(this).hide().css('opacity',1)});
            return false;
        }
        if ( !emailReg.test(email) ) {
            $('#blad_email_err').show().animate({opacity:1},3000).animate({opacity:0},1000,'linear',function(){$(this).hide().css('opacity',1)});
            return false;
        }
        if( firma.length == 0 ){
            $('#firma').val("");
            $('#blad_firma').show().animate({opacity:1},3000).animate({opacity:0},1000,'linear',function(){$(this).hide().css('opacity',1)});
            return false;
        }
    });

    $("#form .otworz, #form .open").toggle(
        function(){
            $(this).parent().animate({left: "0px"}, 500 );
            $(this).addClass("zamknij");
            return false;
        },
        function(){
            var width = $(this).parent().outerWidth() - $(this).width();

            $(this).parent().animate({left: "-240px"}, 500 );
            $(this).removeClass("zamknij");
            return false;
        }
    );

    $('#search').keyup(function() {
        search_product();
    });
});

var search_content = "";

function search_product() {
    if(search_content == "") {
        search_content = $('.tekst').html();
    }

    var search_val = $('#search').val().trim();
    if(search_val.length >= 3) {
        $.ajax({
            url: '/'+ current_language + '/opinions/search/'+ search_val +'/',
            success: function(data) {
                $('.tekst').html(data);
                $('.pagination').hide();
            }
        });
    } else if(search_val.length == 0) {
        $('.tekst').html(search_content);
        $('.pagination').show();
    }
}
