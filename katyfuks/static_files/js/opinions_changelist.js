$(document).ready(function() {
    // Set this to the name of the column holding the parent
    parent_field = 'parent';
    // Set this to the name of the column holding the position
    pos_field = 'position';

    // Determine the column number of the parent field
    parent_col = null;
    // Determine the column number of the position field
    pos_col = null;

    cols = $('#result_list tbody tr:first').children()

    for (i = 0; i < cols.length; i++) {
        var inputs = $(cols[i]).find('input[name*=' + pos_field + ']');

        if (inputs.length > 0) {
            // Found!
            pos_col = i;
            $('#result_list tbody').find('input[name*=' + pos_field + ']').addClass('position_field').parent().addClass('position_container');
            break;
        }
    }

    if (pos_col == null) {
        return;
    }

    for (i = 0; i < cols.length; i++) {
        var selects = $(cols[i]).find('select[name*=' + parent_field + ']');

        if (selects.length > 0) {
            // Found!
            parent_col = i;
            $('#result_list tbody').find('select[name*=' + parent_field + ']').addClass('parent_field');
            break;
        }
    }

    if (parent_col == null) {
        return;
    }

    restructure_table(parent_col, cols.length);

    // Some visual enhancements
    header = $('#result_list thead tr').children()[pos_col]
    $(header).css('width', '5em')
    $(header).children('.text').children('a').text('#')

    // Hide position field
    $('#result_list tbody tr').each(function(index) {
        pos_td = $(this).children()[pos_col]
        input = $(pos_td).children('input').first()
        //input.attr('type', 'hidden')
        input.hide()

        label = $('<strong>' + input.attr('value') + '</strong>')
        $(pos_td).append(label)
    });

    // Hide parent field
    $('.parent_field').parent().hide();
    var parent_header = $('#result_list thead tr').children()[parent_col];
    $(parent_header).hide();

    // Determine sorted column and order
    sorted = $('#result_list thead th.sorted')
    sorted_col = $('#result_list thead th').index(sorted)
    sort_order = sorted.hasClass('descending') ? 'desc' : 'asc';

    if (sorted.length > 1) {
        // Sorting is not only on position field, bail out
        console.info("Sorting is not only on position field");
        return;
    }

    if (sorted_col != pos_col) {
        // Sorted column is not position column, bail out
        console.info("Sorted column is not %s, bailing out", pos_field);
        return;
    }

    $('#result_list tbody tr').css('cursor', 'move')

    // Make tbody > tr sortable
    $('#result_list > tbody, .subcategory_container > tbody').addClass('drop_zone');
    $('#result_list > tbody, .subcategory_container > tbody').sortable({
        connectWith: ".drop_zone",
        appendTo: 'parent',
        cursor: 'move',
        items: '> tr:not(.not_move)',
        revert: true,
        scroll: true,
        tolerance: 'intersect',
        placeholder: 'sortable-placeholder',
        forcePlaceholderSize: true,
        update: function(event, ui) {
            item = ui.item
            items = $('#result_list > tbody').find('tr:not(.not_move)').get()

            if (sort_order == 'desc') {
                // Reverse order
                items.reverse()
            }

            $(items).each(function(index) {
                pos_td = $(this).find('.position_field').parent();
                input = $(pos_td).children('input').first();
                label = $(pos_td).children('strong').first();

                input.attr('value', index + 1);
                label.text(index + 1);
            });

            var new_parent_id = 0;
            if(item.parents('table').hasClass('subcategory_container')) {
                new_parent_id = item.parents('table').attr('data-parent');

                item_childrens = item.find('.subcategory_container > tbody > tr:not(.not_move)');
                $(item_childrens).each(function(index) {
                    $(this).find('.parent_field').val(new_parent_id);
                    $(this).appendTo(item.parent());
                });
            }
            item.find('.parent_field').val(new_parent_id);

            update_row_classes();
        }
    });
});

function update_row_classes() {
    // Update row classes
    $('#result_list > tbody > tr, .subcategory_container > tbody > tr').removeClass('row1').removeClass('row2');
    $('#result_list > tbody > tr:not(.not_move):even').addClass('row1');
    $('.subcategory_container > tbody > tr:not(.not_move):even').addClass('row1');
    $('#result_list > tbody > tr:not(.not_move):odd').addClass('row2');
    $('.subcategory_container > tbody > tr:not(.not_move):odd').addClass('row2');
}

function restructure_table(parent_col, cols_count) {
    var parent_columns = $('#result_list tbody tr').children()[parent_col];

    $('#result_list .parent_field').each(function() {
        var parent_id = $(this).val();
        var parent_tr = $(this).parents('tr');
        var parent_tr_class = parent_tr.attr('class');
        var category_id = parent_tr.find('.action-select').val();

        parent_tr.children('th:last').append('<table class="subcategory_container" data-parent="'+ category_id +'"><tbody><tr class="not_move"><td colspan="'+ cols_count +'"></td></tr></tbody></table>');

        if(parent_id != "" || parent_id != 0) {
            $(".subcategory_container[data-parent="+ parent_id +"] > tbody").append(parent_tr);
        }
    });

    $('#result_list > tbody').append('<tr class="not_move"><td colspan="'+ cols_count +'"></td></tr>');

    update_row_classes();
}
