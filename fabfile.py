from fabric.api import *


def production():
    env.hosts = ['kudlaty@kudlaty.webfactional.com:22']
    env['webapp_dir'] = '~/webapps/k_oriflame/'
    env['dir'] = '~/webapps/k_oriflame/katyfuks/'
    env['venv'] = '~/virtual/k_oriflame/bin/activate'


def update():
    "Updates the repository, and restart apache"
    with cd(env['dir']):
        run("git pull origin master")
        run("source %s && pip install -r req.txt" % (env['venv']))
        run("source %s && ./manage.py migrate" % (env['venv']))

    with cd(env['webapp_dir']):
        run("source %s && . apache2/bin/restart" % (env['venv']))


def update_static():
    "Updates static files"
    with cd(env['dir']):
        run("source %s && ./manage.py collectstatic -cl" % (env['venv']))
