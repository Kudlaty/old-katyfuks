import datetime

from django.db import models
from django.utils.safestring import mark_safe


class Category(models.Model):
    parent_category = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children'
    )
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    position = models.IntegerField(null=True, blank=True)

    products = models.ManyToManyField('Product', blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            try:
                last = model.objects.all().order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                self.position = 1

        return super(Category, self).save(*args, **kwargs)

    class Meta:
        ordering = ['position']
        verbose_name_plural = 'Categories'


class Product(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    code = models.BigIntegerField()
    image = models.ImageField(upload_to='uploads/products')
    description = models.TextField(null=True, blank=True)
    status = models.BooleanField(default=True)

    categories = models.ManyToManyField(Category, through=Category.products.through, blank=True)

    def __unicode__(self):
        return self.name

    def get_description(self):
        return mark_safe(self.description)

    def admin_thumbnail(self):
        return u'<img class="thumbnail" src="%s" />' % (self.image.url)
    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True


class ProductOpinion(models.Model):
    product_id = models.ForeignKey(Product, null=True, blank=True)
    author = models.CharField(max_length=255)
    mail = models.EmailField(max_length=255, null=True, blank=True)
    opinion = models.TextField()
    date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.author

    def get_opinion(self):
        return mark_safe(self.opinion)
