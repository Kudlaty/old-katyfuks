from django.views.generic import FormView, ListView, DetailView
from django.views.generic.edit import FormMixin, ModelFormMixin, BaseCreateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db.models import Q

from opinions.models import Product, ProductOpinion
from opinions.forms import OpinionForm


class ProductListView(ListView):
    queryset = Product.objects.filter(status=True).order_by('-pk')
    template_name = "product_list.html"
    context_object_name = "products"
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = {
            'active_menu': "product_list",
        }
        context.update(kwargs)

        return super(ProductListView, self).get_context_data(**context)


class ProductSearchView(ListView):
    queryset = Product.objects.filter(status=True).order_by('-pk')
    template_name = "_list.html"
    context_object_name = "products"

    def get_queryset(self):
        filters = self.kwargs
        model = Product.objects.filter(status=True).order_by('-pk')

        if len(filters):
            model = Product.objects.filter(status=True).filter(Q(name__icontains=filters['search']) | Q(code__startswith=filters['search'])).order_by('-pk')

        return model


class OpinionListMixin(object):
    def get_context_data(self, **kwargs):
        context = {
            'opinion_list': ProductOpinion.objects.filter(product_id=kwargs['object'].pk).order_by('-date')
        }
        context.update(kwargs)

        return super(OpinionListMixin, self).get_context_data(**context)


class OpinionFormMixin(BaseCreateView):
    form_class = OpinionForm
    success_url = None

    def get_context_data(self, **kwargs):
        context = {
            'form': self.form_class,
        }
        context.update(kwargs)

        return super(OpinionFormMixin, self).get_context_data(**context)

    def form_valid(self, form):
        product_object = Product.objects.get(pk=self.kwargs['pk'])
        form = form.save(commit=False)
        form.product_id = product_object
        form = form.save()

        return HttpResponseRedirect(
            reverse(
                "product_details",
                kwargs={
                    'pk': product_object.pk,
                    'slug': product_object.slug
                }
            )
        )


class ProductDetailsView(OpinionListMixin, OpinionFormMixin, DetailView):
    queryset = Product.objects.filter(status=True)
    template_name = "product.html"
    context_object_name = "product"

    def get_context_data(self, **kwargs):
        context = {
            'active_menu': "product_list",
        }
        context.update(kwargs)

        return super(ProductDetailsView, self).get_context_data(**context)

