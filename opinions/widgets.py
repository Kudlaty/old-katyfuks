from django.forms import CheckboxSelectMultiple, CheckboxInput
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode
from django.utils.html import conditional_escape


class CheckboxSelectMultipleWidget(CheckboxSelectMultiple):
    def __init__(self, attrs=None, choices=(), model=None):
        self.model = model
        super(CheckboxSelectMultipleWidget, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        output = []

        self.model_query = self.model.objects.filter(parent_category=None)

        output = self.make_html_list(output, self.model_query, name, value, attrs, child=False)

        return mark_safe(u'\n'.join(output))

    def make_html_list(self, output, options, name, value, attrs=None, child = False):
        ul_class = 'parent'
        if child:
            ul_class = 'children'
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])

        output.append(u'<ul class="%s">' % ul_class)

        for i, option in enumerate(options):
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''

            option_value = option.pk
            option_label = force_unicode(option)
            option_childrens = option.children.all()

            cb = CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            option_value = force_unicode(option_value)
            rendered_cb = cb.render(name, option_value)
            option_label = conditional_escape(option_label)
            output.append(u'<li>')
            output.append(u'<div class="option_header"><label%s>%s %s</label></div>' % (label_for, rendered_cb, option_label))

            if len(option_childrens) and child is False:
                attrs['id'] = final_attrs['id']
                output = self.make_html_list(output, option_childrens, name, value, attrs, child=True)

            output.append(u'</li>')
        output.append(u'</ul>')

        return output

    class Media:
        css = {
            'all': ('css/admin/checkbox_widget.css',)
        }
        js = (
            'js/admin/checkbox_widget.js',
        )

