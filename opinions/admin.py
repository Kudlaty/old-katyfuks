from django import forms
from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationTabularInline, TranslationStackedInline

from opinions.models import Category, Product, ProductOpinion
from opinions.widgets import CheckboxSelectMultipleWidget


class CategoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['parent_category'].queryset = Category.objects.exclude(
            id__exact=self.instance.id).filter(parent_category=None)

    class Meta:
        model = Category


class CategoryAdmin(TranslationAdmin):
    list_display = ('position', 'parent_category', 'name',)
    list_editable = ('position', 'parent_category',)
    list_display_links = ('name',)
    prepopulated_fields = {'slug_pl': ('name_pl',), 'slug_en': ('name_en',)}
    fieldsets = (
        (None, {
            'fields': ('parent_category', 'name', 'slug', 'products', 'position')
        }),
    )
    filter_horizontal = ('products',)

    form = CategoryForm

    class Media:
        css = {
            'all': ('css/admin/opinions_changelist.css',)
        }
        js = (
            'js/jquery-1.9.1.min.js',
            'js/jquery-ui-1.10.1.min.js',
            'js/opinions_changelist.js',
        )


class ProductAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductAdminForm, self).__init__(*args, **kwargs)
        self.fields['categories'].queryset = Category.objects.all()

    class Meta(object):
        model = Product
        widgets = {
            'categories': CheckboxSelectMultipleWidget( model=Category ),
        }

class ProductAdmin(TranslationAdmin):
    list_display = ('admin_thumbnail', 'name', 'code', 'status')
    search_fields = ('name', 'code', 'description')
    list_display_links = ('admin_thumbnail', 'name')
    list_filter = ('status',)
    prepopulated_fields = {'slug_pl': ('name_pl',), 'slug_en': ('name_en',)}
    fieldsets = (
        (None, {
            'fields': ('categories', 'name', 'slug', 'code', 'image', 'description', 'status')
        }),
    )

    form = ProductAdminForm

    class Media:
        js = (
            'js/jquery-1.9.1.min.js',
            'js/jquery-ui-1.10.1.min.js',
            'js/tiny_mce/tiny_mce.js',
            'js/tiny_mce/textareas.js',
        )


class ProductOpinionAdmin(admin.ModelAdmin):
    list_display = ('author', 'mail', 'date')
    search_fields = ('author', 'mail', 'opinion')
    list_filter = ('date',)

    class Media:
        js = (
            'js/tiny_mce/tiny_mce.js',
            'js/tiny_mce/textareas.js',
        )


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductOpinion, ProductOpinionAdmin)
