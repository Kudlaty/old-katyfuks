from django.conf.urls import patterns, include, url

from opinions.views import ProductListView, ProductSearchView, ProductDetailsView

urlpatterns = patterns('',
    url(r'^$', ProductListView.as_view(), name="product_list"),
    url(r'^search/(?P<search>.*)/$', ProductSearchView.as_view(), name="product_search"),
    url(r'^(?P<pk>\d+)/(?P<slug>.*)/$', ProductDetailsView.as_view(), name="product_details"),
)
