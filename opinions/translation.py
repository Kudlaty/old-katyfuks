from modeltranslation.translator import translator, TranslationOptions

from opinions.models import Product, Category


class ProductTransOpt(TranslationOptions):
    fields = ('name', 'slug', 'description',)


class CategoryTransOpt(TranslationOptions):
    fields = ('name', 'slug',)


translator.register(Category, CategoryTransOpt)
translator.register(Product, ProductTransOpt)