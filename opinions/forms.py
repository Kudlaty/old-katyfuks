from django import forms

from opinions.models import ProductOpinion


class OpinionForm(forms.ModelForm):
    class Meta:
        model = ProductOpinion
        exclude = ('date',)

