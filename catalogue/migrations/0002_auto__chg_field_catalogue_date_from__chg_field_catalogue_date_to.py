# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Catalogue.date_from'
        db.alter_column('catalogue_catalogue', 'date_from', self.gf('django.db.models.fields.DateField')())

        # Changing field 'Catalogue.date_to'
        db.alter_column('catalogue_catalogue', 'date_to', self.gf('django.db.models.fields.DateField')())

    def backwards(self, orm):

        # Changing field 'Catalogue.date_from'
        db.alter_column('catalogue_catalogue', 'date_from', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Catalogue.date_to'
        db.alter_column('catalogue_catalogue', 'date_to', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        'catalogue.catalogue': {
            'Meta': {'ordering': "['-number']", 'object_name': 'Catalogue'},
            'date_from': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'date_to': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {})
        },
        'catalogue.page': {
            'Meta': {'ordering': "['position']", 'object_name': 'Page'},
            'catalogue': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalogue.Catalogue']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['catalogue']