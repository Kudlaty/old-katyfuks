# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Catalogue.publication_from'
        db.add_column('catalogue_catalogue', 'publication_from',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)

        # Adding field 'Catalogue.publication_to'
        db.add_column('catalogue_catalogue', 'publication_to',
                      self.gf('django.db.models.fields.DateField')(default=datetime.date.today),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Catalogue.publication_from'
        db.delete_column('catalogue_catalogue', 'publication_from')

        # Deleting field 'Catalogue.publication_to'
        db.delete_column('catalogue_catalogue', 'publication_to')


    models = {
        'catalogue.catalogue': {
            'Meta': {'ordering': "['-number']", 'object_name': 'Catalogue'},
            'date_from': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'date_to': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'publication_from': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'publication_to': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'})
        },
        'catalogue.page': {
            'Meta': {'ordering': "['position']", 'object_name': 'Page'},
            'catalogue': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalogue.Catalogue']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'position': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['catalogue']