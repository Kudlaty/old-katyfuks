# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Catalogue'
        db.create_table('catalogue_catalogue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('date_from', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('date_to', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal('catalogue', ['Catalogue'])

        # Adding model 'Page'
        db.create_table('catalogue_page', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('catalogue', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalogue.Catalogue'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('catalogue', ['Page'])


    def backwards(self, orm):
        # Deleting model 'Catalogue'
        db.delete_table('catalogue_catalogue')

        # Deleting model 'Page'
        db.delete_table('catalogue_page')


    models = {
        'catalogue.catalogue': {
            'Meta': {'ordering': "['-number']", 'object_name': 'Catalogue'},
            'date_from': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'date_to': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {})
        },
        'catalogue.page': {
            'Meta': {'ordering': "['position']", 'object_name': 'Page'},
            'catalogue': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalogue.Catalogue']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['catalogue']