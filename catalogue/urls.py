from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from catalogue.views import ContactView

urlpatterns = patterns('',
    url(r'^thanks/', TemplateView.as_view(template_name="thanks.html"), name="thanks"),

    url(r'^$', ContactView.as_view(), name="homepage"),
)
