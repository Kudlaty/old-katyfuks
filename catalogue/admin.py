import tempfile
import base64

from django import forms
from django.contrib import admin
from django.conf.urls import patterns
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core.files.uploadedfile import SimpleUploadedFile

from .models import Catalogue, Page
from .forms import PageForm
from katyfuks import settings


class PageInline(admin.StackedInline):
    model = Page
    extra = 0

    class Media:
        css = {
            'all': ('css/admin/catalogue_change.css',)
        }
        js = (
            'js/jquery-1.9.1.min.js',
            'js/jquery-ui-1.10.1.min.js',
            'js/admin/catalogue_change.js',
        )

class CatalogueAdmin(admin.ModelAdmin):
    list_display = ('number', 'date_from', 'date_to', 'publication_from', 'publication_to',)
    list_display_links = ('number', 'date_from', 'date_to', 'publication_from', 'publication_to',)
    inlines = [ PageInline, ]

    def get_urls(self):
        urls = super(CatalogueAdmin, self).get_urls()
        page_urls = patterns('',
            (r'^upload_image/$', self.admin_site.admin_view(self.upload_image))
        )

        return page_urls + urls

    def upload_image(self, request):
        img = None
        data_return = "Error"

        if request.method == 'POST':
            old = request.POST.get('old', 0)
            delete_page = request.POST.get('delete_page', 0)
            page_id = request.POST.get('page_id', 0)
            file_src = request.POST.get('file', 0)
            file_name = request.POST.get('file_name', 0)
            position = request.POST.get('position', 0)
            catalogue_id = request.POST.get('catalogue', 0)

            if old:
                page = 0
                try:
                    page = Page.objects.get(pk=page_id)
                except:
                    img = 0
                    data_return = 0

                if page:
                    if delete_page:
                        page.delete()
                        img = 0
                        data_return = 0
                    else:
                        page.position = position
                        page.save()
                        img = page
            else:
                file_src = file_src.split('base64,')
                file_binary = base64.b64decode(file_src[1])
                file_mime = file_src[0].replace('data:', '').replace(';', '')

                newfile = SimpleUploadedFile(file_name, file_binary)
                request_file = {'image': newfile}

                form = PageForm(request.POST, request_file)
                if form.is_valid():
                    img = form.save()

        if img is not None and img != 0:
            data_return = img.pk

        return HttpResponse(data_return, content_type="text/plain")


admin.site.register(Catalogue, CatalogueAdmin)
