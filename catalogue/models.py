import os, datetime

from django.db import models
from django.utils.safestring import mark_safe


class Catalogue(models.Model):
    number = models.IntegerField()
    date_from = models.DateField(default=datetime.date.today)
    date_to = models.DateField(default=datetime.date.today)
    publication_from = models.DateField(default=datetime.date.today)
    publication_to = models.DateField(default=datetime.date.today)

    def __unicode__(self):
        return "Catalogue: " + str(self.number)

    class Meta:
        ordering = ['-number']


def get_page_upload_path(instance, filename):
    return os.path.join("uploads", "catalogue", str(instance.catalogue.number), filename)

class Page(models.Model):
    catalogue = models.ForeignKey(Catalogue)
    image = models.ImageField(upload_to=get_page_upload_path)
    position = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return "Page: " + str(self.position)

    def save(self, *args, **kwargs):
        model = self.__class__

        if self.position is None:
            try:
                last = model.objects.filter(catalogue=self.catalogue).order_by('-position')[0]
                self.position = last.position + 1
            except IndexError:
                self.position = 1

        return super(Page, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path

        super(Page, self).delete(*args, **kwargs)
        storage.delete(path)

    class Meta:
        ordering = ['position']
