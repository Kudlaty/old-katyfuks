# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.core.mail import EmailMessage, send_mail
from email.mime.text import MIMEText

from katyfuks.settings import DEFAULT_FROM_EMAIL, CONTACT_EMAIL
from .models import Page


class ContactForm(forms.Form):
    name = forms.CharField()
    surename = forms.CharField()
    phone = forms.CharField()
    mail = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        body = "Kolejna osoba składa zamówienie:\n"
        body += "Imie: %s\n"
        body += "Nazwisko: %s\n"
        body += "Telefon: %s\n"
        body += "E-Mail: %s\n\n"
        body += "Message:\n"
        body += "%s\n\n"
        body += "Pozdrawiamy,\n"
        body += "Strona katyfuks"
        body = body % ( self.cleaned_data['name'], self.cleaned_data['surename'], self.cleaned_data['phone'], self.cleaned_data['mail'], self.cleaned_data['message'] )

        subject = 'Zamówienie z oriflame'

        send_mail(subject, body, DEFAULT_FROM_EMAIL, [CONTACT_EMAIL])


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        #fields = ('catalogue', 'position')
