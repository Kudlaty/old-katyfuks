import datetime

from django.views.generic import FormView

from catalogue.forms import ContactForm
from .models import Catalogue


class ContactView(FormView):
    template_name = "index.html"
    form_class = ContactForm
    success_url = "/thanks/"

    def form_valid(self, form):
        form.send_email()

        return super(ContactView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        try:
            catalogue = Catalogue.objects.filter(publication_from__lte=datetime.date.today).filter(publication_to__gte=datetime.date.today)[0]
        except:
            catalogue = []
        context = {
            'active_menu': "home",
            'catalogue_item': catalogue,
        }
        context.update(kwargs)

        return super(ContactView, self).get_context_data(**context)
